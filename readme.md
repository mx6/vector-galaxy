Galaxy
====================

![Image of Planet](./image/planet.png)

# Instruction

1) The challenge is in the branch "test".

2) You can only modify the file "index.ts" in root folder.

3) All your test should pass succesfully when calling jest with npm.

# Note

- Don't forget to correctly install your project before starting to code.
- The only function called by the tests is "getPlanets", but feel free to create more function if needed.
- The function "getPlanets" should works for the 4 tests at the same time.